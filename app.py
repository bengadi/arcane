#Import dependencies
from flask import make_response
from flask_httpauth import HTTPBasicAuth
from flask_sqlalchemy import SQLAlchemy
from passlib.apps import custom_app_context as pwd_context
from flask import Flask, abort, request, jsonify, g
from flask_marshmallow import Marshmallow
from datetime import datetime
import os
app = Flask(__name__)


app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'
#We are going to use a Flask-SQLAlchemy database to store users and propreties.
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True

# extensions
db = SQLAlchemy(app)
ma = Marshmallow(app)
auth = HTTPBasicAuth()

#The user class. For each user a username and a birthday and a password_hash will be stored.
class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True)
    bday = db.Column(db.DateTime)
    password_hash = db.Column(db.String(64))
    #The hash_password() method takes a plain password as argument and stores a hash of it with the user
    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)
    #The verify_password() method takes a plain password as argument and returns True
    #if the password is correct or False if not.
    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)
    #The datetime_to_str() method takes a string of a birthday and return a datedame format.
    def datetime_to_str(self,datetime_str):
        self.bday = datetime.strptime(datetime_str, '%d/%m/%Y').date()
        
#The Immeuble (properties) class.
class Immeuble(db.Model):
    __tablename__ = 'immeuble'
    id = db.Column(db.Integer, primary_key=True)   
    nom = db.Column(db.String(32), index=True)
    description = db.Column(db.String(64))
    type_de_bien = db.Column(db.String(64))
    ville = db.Column(db.String(64))
    pieces = db.Column(db.Integer)
    caracteristiques = db.Column(db.String(64))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref='immeubles')
    
class UserSchema(ma.ModelSchema):
    class Meta:
        model = User 
class ImmeubleSchema(ma.ModelSchema):
    class Meta:
        model = Immeuble   
   
#This function finds the user by the username, then verifies the password using the verify_password() method.        
@auth.verify_password
def verify_password(username, password):
    user = User.query.filter_by(username = username).first()
    if not user or not user.verify_password(password):
        return False
    g.user = user
    return True

#User Registration:
#If the arguments are valid then a new User instance is created.
@app.route('/arcane/users', methods=['POST'])
def new_user():
    username = request.json.get('username')
    password = request.json.get('password')
    bday = request.json.get('bday')
    if username is None or password is None:
        abort(400) # missing arguments
    if User.query.filter_by(username = username).first() is not None:
        abort(400) # existing user
    user = User(username = username)
    if bday is not None: 
        try:
            user.datetime_to_str(bday)
        except:
            return make_response(jsonify({'error': 'bday error'}), 404)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return jsonify({ 'username': user.username ,'bday': user.bday})

#User update:
#If the arguments are valid then the User instance is modified.
#A user authentication is required.
@app.route('/arcane/users/update_user', methods=['PUT'])
@auth.login_required
def update_user():
    user = g.user
    if not user:
            abort(400)
    username = request.json.get('username')
    bday = request.json.get('bday')
    if User.query.filter_by(username=username).first() is not None:
            abort(400)    # existing user
    if username is not None:
            user.username=username   
    if  bday is not None:
            user.datetime_to_str(bday)
    return jsonify({'username': user.username ,'bday': user.bday})

#The implementation of the get_user endpoint to get user informations.
#A user authentication is required.
@app.route('/arcane/users/<username>')
@auth.login_required
def get_user(username):
    user = User.query.filter_by(username=username).first()
    if not user:
        abort(400)

    return jsonify({'username' : user.username, 'bday' : user.bday})

#The implementation of the get_immeubles endpoint to get all propreties.
@app.route('/arcane/immeubles', methods=['GET'])
def get_immeubles():
    immeubles = Immeuble.query.all()
    immeuble_schema = ImmeubleSchema(many=True)
    output = immeuble_schema.dump(immeubles)
    return jsonify({'immeuble' : output})

#The implementation of the get_immeuble endpoint to get a proprety by id.
@app.route('/arcane/immeubles/<int:immeuble_id>', methods=['GET'])
def get_immeuble(immeuble_id):
    immeuble = Immeuble.query.get(immeuble_id)
    immeuble_schema = ImmeubleSchema()
    output = immeuble_schema.dump(immeuble)
    return jsonify({'immeuble' : output})
# 404 error handler
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

#The implementation of the get_city endpoint to get a proprety by city.
@app.route('/arcane/immeubles/<city>', methods=['GET'])
def get_city(city):
    immeubles = Immeuble.query.filter_by(ville=city)
    immeuble_schema = ImmeubleSchema(many=True)
    output = immeuble_schema.dump(immeubles)
    return jsonify({'immeuble' : output}), 201
    
#The implementation of the create_immeuble endpoint to create a new proprety.
#If the arguments are valid then a new proprety instance is created.
#A user authentication is required.
@app.route('/arcane/immeubles', methods=['POST'])
@auth.login_required
def create_immeuble():
    immeuble = {
        'nom': request.json.get('nom'),
        'description': request.json.get('description'),
        'type_de_bien': request.json.get('type_de_bien'),
        'ville': request.json.get('ville', ""),
        'pieces': request.json.get('pieces'),
        'caracteristiques':request.json.get('caracteristiques')}
    immeuble_new=Immeuble(user=g.user)
    #Simple verification of arguments
    if 'nom' in immeuble and bool(immeuble['nom']):
        immeuble_new.nom=immeuble['nom']
    if 'description' in immeuble and bool(immeuble['description']):
        immeuble_new.description= immeuble['description']
    if 'type de bien' in immeuble and bool(immeuble['type de bien']):
        immeuble_new.type_de_bien= immeuble['type de bien']
    if 'ville' in immeuble and bool(immeuble['ville']):
        immeuble_new.ville= immeuble['ville']
    if 'pieces' in immeuble and bool(immeuble['pieces']):
        immeuble_new.pieces = immeuble['pieces']
    if 'caracteristiques des piece' in immeuble and bool(immeuble['caracteristiques des piece']):
        immeuble_new.caracteristiques= immeuble['caracteristiques des piece']   
    db.session.commit()
    immeuble_schema = ImmeubleSchema()
    output = immeuble_schema.dump(immeuble_new)
    return jsonify({'immeuble' : output}), 201

#The implementation of the update_immeuble endpoint to update a proprety by id.
#A user authentication is required.
@app.route('/arcane/immeubles/<int:immeuble_id>', methods=['PUT'])
@auth.login_required
def update_immeuble(immeuble_id):
    immeuble_old = Immeuble.query.get(immeuble_id)
    immeuble = {
        'nom': request.json.get('nom'),
        'description': request.json.get('description'),
        'type_de_bien': request.json.get('type_de_bien'),
        'ville': request.json.get('ville'),
        'pieces': request.json.get('pieces'),
        'caracteristiques':request.json.get('caracteristiques')}
    if g.user==immeuble_old.user:
            if 'nom' in immeuble and bool(immeuble['nom']):
                immeuble_old.nom=immeuble['nom']
            if 'description' in immeuble and bool(immeuble['description']):
                immeuble_old.description= immeuble['description']
            if 'type de bien' in immeuble and bool(immeuble['type de bien']):
                immeuble_old.type_de_bien= immeuble['type de bien']
            if 'ville' in immeuble and bool(immeuble['ville']):
                immeuble_old.ville= immeuble['ville']
            if 'pieces' in immeuble and bool(immeuble['pieces']):
                immeuble_old.pieces = immeuble['pieces']
            if 'caracteristiques des piece' in immeuble and bool(immeuble['caracteristiques des piece']):
                immeuble_old.caracteristiques= immeuble['caracteristiques des piece']
            immeuble_schema = ImmeubleSchema()
            output = immeuble_schema.dump(immeuble_old)
            return jsonify({'immeuble' : output}), 201
    else:
        return make_response(jsonify({'error': 'you are not the owner'}), 404)       

#The implementation of the delete_immeuble endpoint to delete a proprety.
#A user authentication is required.
@app.route('/arcane/immeubles/<int:immeuble_id>', methods=['DELETE'])
@auth.login_required
def delete_immeuble(immeuble_id):
    immeuble = Immeuble.query.get(immeuble_id)
    if g.user==immeuble.user:
        if not immeuble:
            abort(400)
        db.session.delete(immeuble)
        db.session.commit()
        return jsonify({'result': True})
    else:
        return make_response(jsonify({'error': 'you are not the owner'}), 404)    
        


if __name__ == '__main__':
    if not os.path.exists('db.sqlite'):
        db.create_all()
    app.run(debug=True)