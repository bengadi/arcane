# Project Title

Arcane Test
## Getting Started
The goal of this test is to create a set of microservices. These microservices should allow a user to fill in a property with the following characteristics: name, description, type of property, city, rooms, characteristics of the rooms, owner) and to consult the other properties available on the platform. 

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
we will use `curl`. If you don't have  [curl](https://curl.haxx.se/) installed, go ahead and install it now.

## Quick start
#### Environment
This code is developed using on Python 3.7 and Flask 1.1.


#### Install 
1. Install Flask 1.1:
````bash
pip install Flask
````
2. Create (and activate) a new environment,  with Python 3.7.
	- __Windows__: 
	```
	conda create --name envname python=3.7
	activate envname 
	```
2. Install dependencies :
````bash
pip install -r requirements.txt
````
or
````bash
pip install Flask-WTF
pip install flask_sqlalchemy
pip install flask-httpauth
pip install SQLAlchemy
pip install passlib
pip install flask-marshmallow
pip install marshmallow-sqlalchemy
````
3. Clone the project :
````bash 
git clone https://gitlab.com/bengadi/arcane.git
````
3. Run the API.
Start the web service  by running `app.py`  or `flask run`, you can launch your web browser and type `http://localhost:5000`.
 Then open a new console window and run the following command:
#### Authentication
You need to user your user and password to modifie and delete only your properties but i have the acces to see other users propreties.
#### Test

1. Create a user account, you can change the username and the password with yours.
````bash 
curl -i -H "Content-Type: application/json" -X POST -d "{"""username""":"""user""","""password""":"""password""","""bday""":"""01/05/1997"""}" http://127.0.0.1:5000/arcane/users
````
2. Modify  user informations.
````bash 
curl  -u user:password -i -H "Content-Type: application/json" -X PUT -d "{"""bday""":"""01/05/1996"""}" http://127.0.0.1:5000/arcane/users/update_user
````
3. Display user informations.
````bash 
curl -u user:password -i http://127.0.0.1:5000/arcane/users/user
````
4. Create a property.
````bash 
curl -u user:password -i -H "Content-Type: application/json" -X POST -d "{"""nom""":"""immeuble 6"""}" http://127.0.0.1:5000/arcane/immeubles
````
5. Modify the information of your properties.
````bash 
curl  -u user:password -i -H "Content-Type: application/json" -X PUT -d "{"""description""":"""bel appartement neuf modifie"""}" http://127.0.0.1:5000/arcane/immeubles/3
````
6. Delete a property by id.
````bash 
curl  -u user:password -i -X DELETE http://127.0.0.1:5000/arcane/immeubles/3
````
7. Display properties.
````bash 
curl -i http://127.0.0.1:5000/arcane/immeubles
````
8. Display a property by id.
````bash 
curl -i http://127.0.0.1:5000/arcane/immeubles/2
````
9. Display  properties by city.
````bash 
curl -i http://127.0.0.1:5000/arcane/immeubles/paris
````